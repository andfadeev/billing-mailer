(defproject billing-mailer "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [org.clojure/core.async "0.2.374"]
                 [mount "0.1.6"]
                 [clj-http "2.0.0"]
                 [com.taoensso/timbre "4.1.4"]
                 [org.clojure/tools.nrepl "0.2.11"]
                 [org.clojure/tools.namespace "0.2.11"]
                 [ru.hh.rabbitmq.spring/rabbitmq-client "1.1.1"]
                 [ru.headhunter.jtasks/jtasks-api "0.0.5"]
                 [commons-logging "1.2"]
                 [org.slf4j/slf4j-api "1.7.13"]
                 [org.slf4j/slf4j-simple "1.7.13"]
                 [ru.hh.commons/hh-mail-utils "0.6.3"
                  :exclusions [[commons-logging]
                               [ru.hh.rabbitmq.spring/rabbitmq-client]]]]
  :local-repo "../../.hh-maven/repository"
  :main billing-mailer.main
  )
