(ns billing-mailer.task-processors
  (:require
    [mount.core :refer [defstate]]
    [clojure.core.async :as a]
            [taoensso.timbre :as log]
            [billing-mailer.rabbit-task-handlers :refer [handle-rabbit-task]]
            ))

(defn start-task-processor-thread [chan & [pid]]
  (a/thread
    (loop []
      (log/info "Task processor" pid "is waiting for rabbit task")
      (when-let [task (a/<!! chan)]
        (log/info "Task processor" pid "is processing task" task)
        (try
          (handle-rabbit-task task)
          (catch RuntimeException re
            ;todo уменьшить счетчик и отправить в очередь еще раз
            (log/error "RuntimeException in handle-rabbit-task" re)))
        (recur)
        ))
    (log/info "Task processor" pid "is stopped")
    ))

(defn close-task-channel [chan]
  (log/info "Stopping task processors")
  (a/close! chan))

(defn create-task-chan-with-consumers []
  (let [task-channel (a/chan 10)]
    (log/info "Starting task processors")
    (start-task-processor-thread task-channel 1)
    (start-task-processor-thread task-channel 2)
    task-channel
    ))

(defstate task-channel
          :start (create-task-chan-with-consumers)
          :stop (close-task-channel task-channel))

(defn put-rabbit-task! [task]
  (a/put! task-channel task))
