(ns billing-mailer.rabbit-task-handlers
  (:require
   [clojure.core.async :as a]
   [clojure.xml :as xml]
   [clj-http.client :as client]
   [mount.core :refer [defstate]]
   [taoensso.timbre :as log]
   [billing-mailer.mail-service :refer [mail-service]]
   [billing-mailer.conf :refer [config]]
   )
  (:import
   (java.util HashMap)
   (ru.hh.mailer.message Priority)
   (ru.hh.mail.model Localization Localization$Lang)
   (java.io ByteArrayInputStream)
   ))

(defmulti handle-rabbit-task (fn [task] (:type task)))

(defmethod handle-rabbit-task
  :notify_bill_creation
  [task]
  (a/go
    (let [params (:params task)
          hhid-resp (:attrs
                     (first
                      (:content
                       (xml/parse
                        (ByteArrayInputStream.
                         (.getBytes
                          (:body
                           (client/get (str (:hhid-service-url config) (get params "recipientHhid"))))))))))
          local (new Localization
                     (Localization$Lang/valueOf Localization$Lang (get params "lang"))
                     (Integer/parseInt (get params "siteId"))
                     (get params "hostName"))]
      (.sendTemplatedMap mail-service
                         (:email hhid-resp)
                         (str (:last hhid-resp) (:first hhid-resp))
                         (get params "templateCode")
                         (new HashMap params)
                         Priority/HIGH
                         local)))
  )

(defmethod handle-rabbit-task
  :notify_new_bill_by_non_resident
  [task]
  (a/go
    (let [params (:params task)
          local (new Localization
                     (Localization$Lang/valueOf Localization$Lang (get params "lang"))
                     (Integer/parseInt (get params "siteId"))
                     (get params "hostName"))
          recepient-email (get params "recipientEmail")]
      (.sendTemplatedMap mail-service
                         recepient-email
                         recepient-email
                         (get params "templateCode")
                         (new HashMap params)
                         Priority/HIGH
                         local)))
  )

(defmethod handle-rabbit-task
  :notify_about_bill_discard
  [task]
  (a/go (log/info "Handling task with type" (:type task))))

(defmethod handle-rabbit-task
  :default
  [task]
  (log/warn "No handler for task type" (:type task)))
