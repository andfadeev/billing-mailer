(ns billing-mailer.main
  (:require
    [mount.core :as mount]
    [taoensso.timbre :as log]
    [billing-mailer.nrepl :refer :all]
    [billing-mailer.conf :refer :all]
    [billing-mailer.rabbit-task-handlers :refer :all]
    [billing-mailer.task-processors]
    [billing-mailer.rabbit-receivers]
    [clojure.tools.namespace.repl :as tools])
  (:gen-class))

;; todo уменьшить счетчик и отправить в очередь еще раз

(defn -main [& args]
  (log/info "Starting app")
  (mount/start)
  (.addShutdownHook
    (Runtime/getRuntime)
    (new Thread (fn []
                  (log/info "Shutting down app")
                  (mount/stop)
                  (log/info "Bye!")
                  ))))
