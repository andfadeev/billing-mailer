(ns billing-mailer.mail-service
  (:require
   [taoensso.timbre :as log]
   [mount.core :refer [defstate]]
   [billing-mailer.conf :refer [config]]
   )
  (:import (java.util Properties)
           (ru.hh.mail.utils MailClientFactorySettings MailClientFactory MailServiceImpl)))

(defn create-mail-service [props]
  (log/info "Create mail service" props)
  (let [properties (Properties.)
        _ (.putAll properties props)]
    (-> properties
        MailClientFactorySettings.
        MailClientFactory.
        MailServiceImpl.)
    )
  )

(defstate mail-service :start (create-mail-service (:mail-service config)))
