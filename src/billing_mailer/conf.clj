(ns billing-mailer.conf
  (:require
   [taoensso.timbre :as log]
   [clojure.edn :as edn]
   [mount.core :refer [defstate]]
   ))

(defn load-config [path]
  (log/info "Loading config from" path)
  (-> path
      slurp
      edn/read-string))

(defstate config
  :start (load-config "resources/config.edn"))
