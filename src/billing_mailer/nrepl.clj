(ns billing-mailer.nrepl
  (:require
    [billing-mailer.conf :refer [config]]
    [mount.core :refer [defstate]]
    [taoensso.timbre :as timbre]
    [clojure.tools.nrepl.server :refer [start-server stop-server]]
    ))

(defn start-nrepl [{:keys [port]}]
  (timbre/info "Starting nREPL on port" port)
  (start-server :port port))

(defstate nrepl
          :start (start-nrepl (:nrepl config))
          :stop (stop-server nrepl))