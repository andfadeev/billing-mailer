(ns billing-mailer.rabbit-receivers
  (:require
    [mount.core :refer [defstate]]
    [taoensso.timbre :as log]
    [billing-mailer.conf :refer [config]]
    [billing-mailer.task-processors :refer [put-rabbit-task!]]
    )
  (:import (ru.hh.rabbitmq.spring.receive GenericMessageListener)
           (ru.hh.jlogic.rabbit RabbitTask)
           (java.util Properties)
           (ru.hh.rabbitmq.spring ClientFactory)
           (org.springframework.amqp.support.converter Jackson2JsonMessageConverter)))

(defn create-listener []
  (reify
    GenericMessageListener
    (handleMessage [this message]
      (when (instance? RabbitTask message)
        (log/info "Message from rabbit: " message)
        (let [task {:type         (keyword (.getType message))
                    :params       (into {} (.getParameters message))
                    :retries-left (.getRetriesLeft message)}]
          (put-rabbit-task! task)
          )
        ))
    ))

(defn start-receivers [receivers-props]
  (log/info "Starting receivers" receivers-props)
  (doall (map (fn [receiver-props]
                (let [properties (Properties.)
                      _ (.putAll properties receiver-props)]
                  (.start (.withListenerAndConverter
                            (.createReceiver (ClientFactory. properties))
                            (create-listener)
                            (Jackson2JsonMessageConverter.)))))
              receivers-props))
  )

(defn stop-receivers [receivers]
  (doall (map (fn [receiver]
                (log/info "Stopping receiver" receiver)
                (.stop receiver))
              receivers)))

(defstate receivers
          :start (start-receivers (:receivers config))
          :stop (stop-receivers receivers))
